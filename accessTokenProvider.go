package gooauth

import (
	"crypto/rsa"
	"github.com/lestrrat/go-jwx/jwk"
	"sync"
	"time"
)

type AccessTokenResponse struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int64  `json:"expires_in"`
	ExpiresOn   int64  `json:"expires_on"`
	NotBefore   int64  `json:"not_before"`
	Resource    string `json:"resource:"`
}

type AccessTokenErrorResponse struct {
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
	ErrorCodes       []int  `json:"error_codes"`
	Timestamp        string `json:"timestamp"`
	TraceId          string `json:"trace_id"`
	CorrelationId    string `json:"correlation_id"`
}

// AuthenticationProviderType is the type of authentication token provider required, based on the authorization provider.
type AuthenticationProviderType string

const (
	AP_Auth0                = "auth0" // AP_Auth0 is an access token provider for authenticating against Auth0 (https://autho.com)
	AP_AzureActiveDirectory = "aad"   // AP_AzureActiveDirectory is an access token provider for authenticating against Microsoft Azure Active Directory (https://portal.azure.com)
)

// Config is the configuration to be used when creating an AccessTokenProvider
type Config struct {
	ProviderType          AuthenticationProviderType `json:"providerType" yaml:"providerType"`                   // ProviderType is the type of provider based on the authorization provider to support.
	ClientId              string                     `json:"clientId" yaml:"clientId"`                           // ClientId is the unique ID of the client software within the tenant.
	ClientSecret          string                     `json:"clientSecret" yaml:"clientSecret"`                   // ClientSecret is the secret key of the client software to use for authentication within the tenant.
	Domain                string                     `json:"domain" yaml:"domain"`                               // Domain is the domain from which to request authentication. If this is a configuration for user login rather than machine to machine, the port that will be used for serving authentication callback from the provider.
	UserLoginCallbackPort int                        `json:"userLoginCallbackPort" yaml:"userLoginCallbackPort"` // UserLoginCallbackPort is the port that will be used to access on localhost for callback.
	UseHttp               bool                       `json:"useHTTP" yaml:"useHTTP"`                             // UseHttp indicates that http should be used rather than https.  This is for development testing only and not recommended for production use.
}

// AccessTokenProvider is an authentication provider used for authenticating against or validating authentication
// against a specific authorization provider.
type AccessTokenProvider interface {
	// authenticateUser tries to authenticate a user using http page callback
	authenticateUser(resource string) (*AccessTokenResponse, error)
	getAuthenticationProviderType() AuthenticationProviderType
	getWellKnown() ([]byte, error)
	// obtrainMtMToken tries to get a machine to machine access token.
	obtainMtMToken(resource string) (*AccessTokenResponse, error)
	isMachineToMachine() bool
	isUserAuthenticated() bool
}

type tokenCacheStruct struct {
	cache map[string]*AccessTokenResponse
	sync.Mutex
}

var tokenCache tokenCacheStruct

func init() {
	tokenCache.cache = make(map[string]*AccessTokenResponse)
}

// NewProvider obtains a new access token provider based on the given configuration.
func NewProvider(cfg *Config) AccessTokenProvider {
	switch cfg.ProviderType {
	case AP_Auth0:
		provider := auth0AccessTokenProvider{
			clientId:              cfg.ClientId,
			domain:                cfg.Domain,
			clientSecret:          cfg.ClientSecret,
			userLoginCallbackPort: cfg.UserLoginCallbackPort,
			useHttp:               cfg.UseHttp,
		}
		return provider
	case AP_AzureActiveDirectory:
		provider := aadAccessTokenProvider{
			clientId:     cfg.ClientId,
			domain:       cfg.Domain,
			clientSecret: cfg.ClientSecret,
		}
		return provider
	default:
		return nil
	}
}

// ObtainAccessToken obtains an access token for the client to use when communicating with the given resource.
func ObtainAccessToken(provider AccessTokenProvider, resource string) (string, error) {
	tokenCache.Mutex.Lock()
	defer tokenCache.Mutex.Unlock()

	// If there is a valid cert in the cache, use it.
	resp, exists := tokenCache.cache[resource]
	if exists {
		// See if the cert has expired
		now := time.Now().Unix()
		if now >= resp.ExpiresOn {
			exists = false
		}
	}
	if exists {
		return resp.AccessToken, nil
	}

	// Query for a new token
	var err error
	if provider.isMachineToMachine() {
		resp, err = provider.obtainMtMToken(resource)
	} else {
		resp, err = provider.authenticateUser(resource)
	}
	if err != nil {
		return "", err
	} else {
		// Cache the token
		tokenCache.cache[resource] = resp
		return resp.AccessToken, nil
	}
}

// ObtainSigningKeys fetches the signing keys for the authentication authority of the provider useful for validating
// tokens.  Only RSA keys are currently supported.
func ObtainSigningKeys(provider AccessTokenProvider) (map[string]interface{}, error) {
	body, err := provider.getWellKnown()
	if err != nil {
		return nil, err
	}
	keys := make(map[string]interface{})
	set, _ := jwk.Parse(body)
	for _, key := range set.Keys {
		publicKey, _ := key.Materialize()
		if publicKey, ok := publicKey.(*rsa.PublicKey); ok {
			keys[key.KeyID()] = publicKey
		} // else, not an rsa key
	}
	return keys, nil
}
