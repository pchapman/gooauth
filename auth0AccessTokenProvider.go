package gooauth

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

// In both of the below URLs, %s should be tenant ID such as precisionhawk.auth0.com
const auth0TokenURL = "%s://%s/oauth/token"
const auth0KeysURL = "%s://%s/.well-known/jwks.json"

type auth0AccessTokenProvider struct {
	authServer            *auth0UserAuthServer
	clientId              string
	domain                string
	clientSecret          string
	userLoginCallbackPort int
	useHttp               bool
}

func (provider auth0AccessTokenProvider) schema() string {
	if provider.useHttp {
		return "http"
	} else {
		return "https"
	}
}

func (provider auth0AccessTokenProvider) obtainMtMToken(resource string) (*AccessTokenResponse, error) {

	url := fmt.Sprintf(auth0TokenURL, provider.schema(), provider.domain)
	payload := strings.NewReader(
		fmt.Sprintf(
			"{\"client_id\":\"%s\",\"client_secret\":\"%s\",\"audience\":\"%s\",\"grant_type\":\"client_credentials\"}",
			provider.clientId, provider.clientSecret, resource))

	req, _ := http.NewRequest(http.MethodPost, url, payload)
	req.Header.Add("content-type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	} else {
		defer resp.Body.Close()
		atresp := new(AccessTokenResponse)
		data, _ := ioutil.ReadAll(resp.Body)
		if resp.StatusCode != 200 {
			eresp := new(AccessTokenErrorResponse)
			err = json.Unmarshal(data, &eresp)
			if err == nil {
				log.Printf("Error from token request:\t%s\t%s", eresp.Error, eresp.ErrorDescription)
				return nil, fmt.Errorf("%s", eresp.Error)
			} else {
				return nil, err
			}
		} else {
			err = json.Unmarshal(data, &atresp)
			if err != nil {
				log.Printf("Error obtaining access token for resource %s: %s\n", resource, err)
				return nil, err
			} else {
				//				log.Printf("GOWINDAMS: Successfully obtained access token for resource %s: %+v\n", resource, atresp)
				return atresp, nil
			}
		}
	}
}

func (provider auth0AccessTokenProvider) getWellKnown() ([]byte, error) {
	resp, err := http.Get(fmt.Sprintf(auth0KeysURL, provider.schema(), provider.domain))
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("Unable to obtain signing Keys, got response code %d", resp.StatusCode)
	} else {
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		return body, nil
	}
}

/* If there is a client secret, we assume it's server to server.  Otherwise, it must be user authenticated. */

func (provider auth0AccessTokenProvider) isMachineToMachine() bool {
	return provider.clientSecret != ""
}

func (provider auth0AccessTokenProvider) isUserAuthenticated() bool {
	return !provider.isMachineToMachine()
}

func (provider auth0AccessTokenProvider) getAuthenticationProviderType() AuthenticationProviderType {
	return AP_Auth0
}

func (provider auth0AccessTokenProvider) authenticateUser(resource string) (*AccessTokenResponse, error) {
	if provider.authServer == nil {
		provider.authServer = newAuth0UserAuthServer(provider, resource)
	}
	go provider.authServer.startServer()
	resp := provider.authServer.authenticateUser()
	if resp == nil {
		return nil, fmt.Errorf("Unable to authenticate user")
	} else {
		return resp, nil
	}
}
