package main

import (
	"flag"
	"fmt"
	"gitlab.com/pchapman/gooauth"
	"log"
)

func main() {
	audience := flag.String("audience", "", "The audience to request access for")
	clientId := flag.String("clientId", "", "The ID of the client app to use for authentication")
	pType := flag.String("provider", "auth0", "The provider to use for authentication")
	port := flag.Int("port", 9999, "The port to use for callback from the authentication provider")
	domain := flag.String("domain", "", "The domain to authenticate against")

	flag.Parse()

	cfg := gooauth.Config{
		ProviderType:          gooauth.AuthenticationProviderType(*pType),
		ClientId:              *clientId,
		ClientSecret:          "",
		Domain:                *domain,
		UserLoginCallbackPort: *port,
	}

	provider := gooauth.NewProvider(&cfg)
	token, err := gooauth.ObtainAccessToken(provider, *audience)
	if err != nil {
		log.Fatalf("Error getting token: %s\n", err)
	} else {
		fmt.Println(token)
	}
	fmt.Println("Done")
}
