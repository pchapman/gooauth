package gooauth

import (
	"context"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/browser"
	"log"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"text/template"
	"time"
)

type auth0UserAuthServer struct {
	Audience    string
	CallbackURL string
	ClientId    string
	Domain      string

	auth0Provider auth0AccessTokenProvider
	finished      chan *AccessTokenResponse
	signingKeys   map[string]interface{}
	httpServer    *http.Server
}

func newAuth0UserAuthServer(provider auth0AccessTokenProvider, audience string) *auth0UserAuthServer {
	return &auth0UserAuthServer{
		Audience:    audience,
		CallbackURL: fmt.Sprintf("http://localhost:%d/", provider.userLoginCallbackPort),
		ClientId:    provider.clientId,
		Domain:      provider.domain,

		auth0Provider: provider,
		finished:      make(chan *AccessTokenResponse),
	}
}

func (authServer *auth0UserAuthServer) startServer() {
	if authServer.httpServer == nil {
		// Listen for requests
		http.HandleFunc("/report", authServer.reportToken)
		http.HandleFunc("/js/app.js", authServer.viewAppJs)
		http.HandleFunc("/", authServer.viewStatic)
		log.Printf("Listening on port %d", authServer.auth0Provider.userLoginCallbackPort)
		authServer.httpServer = &http.Server{
			Addr:           fmt.Sprintf(":%d", authServer.auth0Provider.userLoginCallbackPort),
			Handler:        nil, // Use default mutex handler
			ReadTimeout:    10 * time.Second,
			WriteTimeout:   10 * time.Second,
			MaxHeaderBytes: 1 << 20,
		}
	}
	authServer.httpServer.ListenAndServe()
}

func (authServer *auth0UserAuthServer) authenticateUser() *AccessTokenResponse {
	// Open the browser
	url := authServer.CallbackURL
	err := browser.OpenURL(url)
	if err != nil {
		fmt.Printf("In order to complete authentication, open your browser and navigate to %s\n", authServer.CallbackURL)
	}

	// Block on the channel, waiting for authentication to occur
	t := <-authServer.finished
	if t != nil {
		log.Printf("Authentication completed")

		// Now that we are authenticated, shut down the http server
		log.Printf("Shutting down HTTP server")
		err := authServer.httpServer.Shutdown(context.Background())
		if err != nil {
			log.Printf("Unable to shutdown http server: %s", err)
		}
		return t
	} else {
		return nil
	}
}

func (authServer *auth0UserAuthServer) viewStatic(w http.ResponseWriter, r *http.Request) {
	log.Printf("Attempting to handle static resource at path '%s' from '%s'.", r.URL.Path, r.RemoteAddr)
	s := r.URL.Path[1:] // Chop off leading /
	if s == "" {
		s = "index.html"
	}
	path := "static/auth0/" + s // Build bindata path
	log.Printf("Attempting to get resource from %s", path)
	content, err := Asset(path)
	if err != nil {
		log.Printf("Static resource at '%s' for path '%s' not found.", path, r.URL.Path)
	}
	ext := filepath.Ext(path)
	if ext != "" {
		ct := mime.TypeByExtension(ext)
		if ext != "" {
			w.Header().Add("content-type", ct)
		}
	}
	w.Write(content)
}

func (authServer *auth0UserAuthServer) viewAppJs(w http.ResponseWriter, r *http.Request) {
	log.Printf("Attempting to handle request for app.js at path '%s' from '%s'.", r.URL.Path, r.RemoteAddr)
	path := "static/auth0/js/app.js"
	content, err := Asset(path)
	if err != nil {
		log.Printf("Static resource at '%s' for path '%s' not found.", path, r.URL.Path)
	}
	template, err := template.New("app.js").Parse(string(content[:]))
	if err != nil {
		log.Fatalf(err.Error())
		os.Exit(1)
	}
	err = template.Execute(w, &authServer)
	if err != nil {
		log.Fatalf(err.Error())
		os.Exit(1)
	}
}

func (authServer *auth0UserAuthServer) reportToken(w http.ResponseWriter, r *http.Request) {
	var resp *AccessTokenResponse
	token := r.URL.Query().Get("token")
	claims := jwt.MapClaims{}
	t, err := jwt.ParseWithClaims(token, claims, authServer.signingKeyForToken)
	if err == nil && t.Valid {
		w.WriteHeader(204)
		w.Write([]byte{}) // empty results
		if f, ok := w.(http.Flusher); ok {
			f.Flush()
		}
		resp = &AccessTokenResponse{
			AccessToken: token,
			TokenType:   "bearer",
			ExpiresIn:   0, // Not populated by Auth0
			ExpiresOn:   int64(claims["exp"].(float64)),
			NotBefore:   0, // Not populated by Auth0
			Resource:    authServer.Audience,
		}
	} else {
		s := fmt.Sprintf("Error parsing token: %s", err)
		w.WriteHeader(500)
		w.Write([]byte(s))
		if f, ok := w.(http.Flusher); ok {
			f.Flush()
		}
		resp = nil
	}
	// give the response time to go to the browser
	time.Sleep(1000 * time.Millisecond)
	log.Printf("Sending token to channel")
	authServer.finished <- resp
	close(authServer.finished)
}

func (authServer *auth0UserAuthServer) signingKeyForToken(token *jwt.Token) (interface{}, error) {
	var err error
	kid := token.Header["kid"]
	if kid == nil {
		return nil, fmt.Errorf("No kid in the header of the token")
	}
	id := fmt.Sprintf("%s", kid)
	if authServer.signingKeys == nil || len(authServer.signingKeys) == 0 {
		authServer.signingKeys, err = ObtainSigningKeys(authServer.auth0Provider)
		if err != nil {
			return nil, err
		}
	}
	key, ok := authServer.signingKeys[id]
	if ok {
		log.Printf("Found signing key for kid %s", id)
		return key, nil
	} else {
		return nil, fmt.Errorf("No signing key found for kid %s", id)
	}
}
