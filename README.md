# GOOAUTH

This library has static html, css and javascript and is stored in bindata.go.  Do not modify bindata.go file.  Instead,
modify the files in the static subdirectory.  To regenerate bindata.go after changes are made, run the  following:

`go-bindata -pkg gooauth $(find static -type d)`
